<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Barcode Generator</title>
</head>
<style>
    html,
    body {
        /* background: #ebebeb;  */
        margin: 0;
        padding: 0;
    }

    body {
        width: 33mm;
        /* height: 15mm; */
        /* border: 1px solid black; */
    }

    div {
        /* margin-bottom: 2px; */
        margin-bottom: 0.05cm;
        /* font-size: 10px; */
        font-size: 0.27cm;
    }

    .barcode {
        /* width: 73px; */
        width: 1.94cm;
        /* margin-top: -10px; */
        /* margin-top: -0.1cm; */
        position: absolute;
        z-index: -1;
    }

    .logo {
        /* width: 25px; */
        width: 0.66cm;
        /* margin-left: 80px; */
        margin-left: 2.2cm;
        /* margin-top: -13px; */
        /* margin-top: -0.35cm; */
    }

    .price {
        /* font-size: 11.5px; */
        font-size: 0.3cm;
        /* margin-left: 80px; */
        margin-left: 2.2cm;
        margin-top: 0px;
    }

    .item-id {
        /* font-size: 10.5px; */
        font-size: 0.34cm;
        /* margin-top: -13px; */
        margin-top: -0.5cm;
        /* margin-left: 23px; */
        margin-left: 0.5cm;
        z-index: 1;
    }

    .col-1 .price, .col-1 .logo {
        margin-left: 2.4cm !important;
    }

    .col-1 .item-id {
        margin-left: 0.75cm !important
    }

    .col-1 .barcode {
        margin-left: 0.211cm !important;
    }

    .col-2 {
        padding-left:0.7cm;
    }

    .col-3 {
        padding-left:0.5cm;
    }

    .item-id.digit-4 {
        margin-top: -0.4cm !important;
        font-size: 0.28cm !important;
        margin-bottom: 0 !important;
    }

    .barcode.digit-4 {
        width: 1.8cm !important;
    }
</style>

<body>
    <table style="border: 0px solid transparent;">
        <thead>
            <th></th>
            <th></th>
            <th></th>
        </thead>
        <tbody>
            @foreach ($retval as $item)
                <tr>
                @for ($i = 0; $i < count($item); $i++)
                    @php
                        $digit = strlen((string) $item[$i]->item_id);
                    @endphp
                    <td class="col-{{$i+1}}">
                        <div>
                            <img src="data:image/png;base64, {{ $item[$i]->code }} " class="barcode{{ $digit == 4 ? ' digit-4' : '' }}" />
                            <img src="{{ asset('img/black.png') }}" class="logo">
                            <div class="price">
                                <b>IDR {{ $item[$i]->price / 1000 }}</b>
                            </div>
                            <div class="item-id{{ $digit == 4 ? ' digit-4' : '' }}">
                                {{ $item[$i]->item_id }}
                            </div>
                        </div>
                    </td>
                @endfor
                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>