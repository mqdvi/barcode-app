<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Upload Excel</title>
</head>
<body>
    <h1>Upload Excel</h1>
    <form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>file excel : </div>
        <input type="file" name="excel">
        <button type="submit">submit</button>
    </form>
</body>
</html>