<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\ItemsImport;
use App\Item;

class ItemController extends Controller
{
    public function barcode($id)
    {
        $items = null;
        $retval = array();
        $n = 0;
        if (strpos($id, "-") && strpos($id, ".")) {
            $item_id = explode('-', $id);
            $count = count($item_id);
            $x = 0;
            for ($i = 0; $i < $count; $i++) {
                for ($j = 0; $j < explode('.', $item_id[$i])[0]; $j++) {
                    $val = Item::where('item_id', explode('.', $item_id[$i])[1])->first();

                    $barcode = new BarcodeGenerator();
                    $barcode->setText((string) $val->item_id);
                    $barcode->setType(BarcodeGenerator::Code128);
                    $barcode->setScale(1);
                    $barcode->setThickness(25);
                    $barcode->setFontSize(0);
                    $code = $barcode->generate();

                    $val->code = $code;
                    $retval[$n][] = $val;


                    if (count($retval[$n]) == 3) {
                        $n++;
                    }
                }

                if (@count($retval[1]) == 3) {
                    break;
                }
            }
        } elseif (strpos($id, '.') && !strpos($id, '-')) {
            $item_id = explode('.', $id)[1];
            $qty = explode('.', $id)[0];
            for ($i = 0; $i < $qty; $i++) {
                $val = Item::where('item_id', $item_id)->first();

                $barcode = new BarcodeGenerator();
                $barcode->setText((string) $val->item_id);
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(1);
                $barcode->setThickness(25);
                $barcode->setFontSize(0);
                $code = $barcode->generate();

                $val->code = $code;
                $retval[$n][] = $val;

                if (count($retval[$n]) == 3) {
                    $n++;
                }

                if (@count($retval[1]) == 3) {
                    break;
                }
            }
        } elseif (strpos($id, '-') && !strpos($id, '.')) {
            $item_id = explode('-', $id);
            $i = 0;
            foreach ($item_id as $item) {
                $val = Item::where('item_id', $item)->first();
                
                $barcode = new BarcodeGenerator();
                $barcode->setText((string) $val->item_id);
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(1);
                $barcode->setThickness(25);
                $barcode->setFontSize(0);
                $code = $barcode->generate();

                $val->code = $code;
                $retval[$n][] = $val;

                if (count($retval[$n]) == 3) {
                    $n++;
                }

                if (@count($retval[1]) == 3) {
                    break;
                }
            }
        } else {
            for ($i = 0; $i < 3; $i++) {
                $val = Item::where('item_id', $id)->first();
                $retval[0][$i] = $val;

                $barcode = new BarcodeGenerator();
                $barcode->setText((string) $val->item_id);
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(1);
                $barcode->setThickness(25);
                $barcode->setFontSize(0);
                $code = $barcode->generate();

                $retval[$n][$i]['code'] = $code;
            }
        }
        
        return view('barcode', compact('retval'));
    }

    public function upload()
    {
        return view('upload');
    }

    public function import(Request $request)
    {
        Excel::import(new ItemsImport, $request->excel);

        return redirect()->back();
    }
}
