<?php

namespace App\Imports;

use App\Item;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ItemsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Item([
            'item_id' => $row['item_id'],
            'name' => $row['name'],
            'price' => $row['price'],
        ]);
    }

    public function headingRow(): int
    {
        return 1;
    }
}
